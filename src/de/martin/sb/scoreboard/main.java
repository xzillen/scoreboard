package de.martin.sb.scoreboard;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scoreboard.Scoreboard;

public class main extends JavaPlugin implements Listener {
	
	Scoreboard sb;
	
	@Override
	public void onEnable() {
		sb = Bukkit.getScoreboardManager().getNewScoreboard();
		
		sb.registerNewTeam("0001Owner");
		sb.registerNewTeam("0002Mod");
		sb.registerNewTeam("0003Spieler");
		
		sb.getTeam("0001Owner").setPrefix("�4Owner �7| �4");
		sb.getTeam("0002Mod").setPrefix("�Mod �7| �c");
		sb.getTeam("0003Spieler").setPrefix("�8");
		
		Bukkit.getPluginManager().registerEvents(this, this);
	}
	
	public void onJoin(PlayerJoinEvent e) {
		setPrefix(e.getPlayer());
	}
	
	@SuppressWarnings({ "deprecation"})
	private void setPrefix(Player p) {
		String team = "";
		if (p.hasPermission("server.owner")) {
			team = "0001Owner";
		}else if(p.hasPermission("server.mod")) {
			team = "0002Mod";
		 } else {
			team = "0003Spieler";
		}
		sb.getTeam(team).addPlayer(p);
		p.setDisplayName(sb.getTeam(team).getPrefix() + p.getName() + "�r");
		
		for(Player all : Bukkit.getOnlinePlayers()) {
			all.setScoreboard(sb);
			p.getPlayer();
		}
		
	}
	
	@EventHandler
	public void on(AsyncPlayerChatEvent e) {
		e.setFormat("�f" + e.getPlayer().getDisplayName() + "�8: �r" + e.getMessage());
	}
}
